setTimeout(() => {
  new MutationObserver(_ => {
    const cover = document.querySelector('div#player-cover img').attributes[
      'src'
    ].value;
    const {
      SNG_ID: id,
      SNG_TITLE: title,
      ART_NAME: artist,
      ALB_TITLE: album
    } = dzPlayer.getCurrentSong();
    const song = {
      id,
      title,
      artist,
      album,
      cover
    };
    console.log(song);
    new Notification(song.title, {
      body: `by ${song.artist}\nfrom ${song.album}`,
      icon: song.cover
    });
  }).observe(document.querySelector('a.player-track-link'), {
    attributes: true,
    subtree: true,
    childList: true
  });
}, 2000);
